import select
import socket
import sys
import csv
import datetime


#cargar el index.html en un buffer: Para mostrar la informacion por web
f = open('index.html', 'r')
index_file_array=f.read()
print(index_file_array)



host=''
port_sensor= 56000
port_nav= 8081
backlog=5
size=1024

#creo el servidor para atender a los drones
server_balizas=socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_balizas.bind((host,port_sensor))
server_balizas.listen(backlog)
print("Servidor escuchando a sensores")

#creo el servidor para el navegador:
server_nav=socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_nav.bind((host,port_nav))
server_nav.listen(backlog)
print("Servidor escuchando a navegadores")

#meto los servidores y la entrada estandar en la lista input
input=[server_balizas,server_nav,sys.stdin]

running=1
while running:
	print("Entrada en el bucle")
	inputready, outputready,exceptready=select.select(input,[],[])
	
	for s in inputready:

	#si llega un cliente baliza (if s==server_balizas), lo aniado a la lista de eventos a atender
		if s==server_balizas:
			print("Informacion enviada desde baliza")
			client, address=server_balizas.accept()
			input.append(client)

			#si llega un cliente de navegador (if s==server_navegadores), lo aniado a la lista de eventos a atender
		elif s==server_nav:
			print("Solicitud de un navegador")
			client, address=server_nav.accept()
			input.append(client)

			#si es un evento de teclado o en general de entrada estandar, corto el bucle
		elif s==sys.stdin:
			junk=sys.stdin.readline()
			running=0
	
	#si no es un servidor ni la entrada estandar, es un cliente:
		else:
			print("Entrada en el else previo al recv")
			data=s.recv(size)
			if data: #si hay algo recibido
			
				if "GET" in data: #si es un navegador
					print ("Navegador conectado")
					client.send("HTTP/1.1 200 OK\n"+"Content-Type: text/html\n"+"\n"+"<html><body>Hello World</body></html>\n");
				#imprimir fichero al navegador, reemplazando las palabras clave por sus valores.
					index_file_array_temp=index_file_array.replace("Variable_1_nombre", Variable_1_nombre)
					index_file_array_temp=index_file_array_temp.replace("Variable_2_nombre", Variable_2_nombre)
					index_file_array_temp=index_file_array_temp.replace("Variable_3_nombre", Variable_3_nombre)
					index_file_array_temp=index_file_array_temp.replace("Variable_1_valor", Variable_1_valor)
					index_file_array_temp=index_file_array_temp.replace("Variable_2_valor", Variable_2_valor)
					index_file_array_temp=index_file_array_temp.replace("Variable_3_valor", Variable_3_valor)
					s.send(index_file_array_temp)
					s.close()
					input.remove(s)
			
		
				#si es un dato de una baliza, guardo el dato en un fichero csv 
				else: 
					#el sensor envia "Variable_1_nombre nombre_de_la_variable Variable_1_valor valor_de_la_variable"
					print(data)
					tupla = data.split(" ")
					print(tupla)
					
					i = datetime.datetime.now() # Obtenemos el tiempo para actualizarlo
					
					col= int(tupla[2])
											
					if "Dron_1" in data:
						datos_actualizar = {1:['01', tupla[1], 'xx', tupla[2]] }

					elif "Dron_2" in data:
						datos_actualizar = {2:['02', tupla[1], 'yy', tupla[2]] }
					
					elif "Dron_3" in data:
						datos_actualizar = {3:['03', tupla[1], 'zz', tupla[2]] }
					
					else: # Dron 4
						datos_actualizar = {4:['04', tupla[1], 'qq', tupla[2]] }
				
				# Sobreescribir el csv
				bottle_list = []
				# Read all data from the csv file.
				
				with open('juego_activo.csv', 'rb') as b:
					bottles = csv.reader(b)
					bottle_list.extend(bottles)
				
				with open('juego_activo.csv', 'wb') as b:
					writer = csv.writer(b)
					for line, row in enumerate(bottle_list):
						data = datos_actualizar.get(line, row)
						writer.writerow(data)
					
			else:
				s.close()
				input.remove(s)
			
	#si es un dato de cliente navegador, abro el fichero index.html, lo guardo en un buffer,
	#reemplazo las variables por los valores que tenga almacenados para ellas, y
	#finalmente imprimo el fichero en el socket del navegador

#server_balizas.close()
#server_nav.close()





"""


if "ABCD" in "xxxxABCDyyyy":
    # whatever


"""
	